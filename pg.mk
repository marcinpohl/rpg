MAKEFLAGS	+= --warn-undefined-variables
MAKEFLAGS	+= --no-builtin-variables
MAKEFLAGS	+= --no-builtin-rules

HOMEDIR := /home/marcin
PGDIR   := $(HOMEDIR)/pg16/
PGBIN   := /usr/lib/postgresql/16/bin/
PGPIDDIR:= /run/postgresql
### https://www.enterprisedb.com/postgres-tutorials/introduction-postgresql-performance-tuning-and-optimization
prepdir:
	sudo mkdir -m750 $(PGPIDDIR)
	sudo chown marcin:postgres $(PGPIDDIR)
prepauth:


###PROPER FLOW for PG>=15:
#CREATE DATABASE EXAMPLE_DB;
#CREATE USER EXAMPLE_USER WITH ENCRYPTED PASSWORD 'Sup3rS3cret';
#GRANT ALL PRIVILEGES ON DATABASE EXAMPLE_DB TO EXAMPLE_USER;
#\c EXAMPLE_DB postgres
# You are now connected to database "EXAMPLE_DB" as user "postgres".
#GRANT ALL ON SCHEMA public TO EXAMPLE_USER;

create:
	### creates DB postgres, and PGUSER marcin
	$(PGBIN)initdb -D $(PGDIR)
poststart:
	### creates DB marcin, owned by PGUSER marcin
	#$(PGBIN)createdb --host=/var/run/postgresql -O marcin -e
	$(PGBIN)createdb --host=$(PGPIDDIR) -O marcin -e
start:
	$(PGBIN)pg_ctl -D $(PGDIR) -l $(PGDIR)logfile start
stop:
	$(PGBIN)pg_ctl -D $(PGDIR) --m fast stop
reload:
	$(PGBIN)pg_ctl -D $(PGDIR) --m fast reload
listdbs:
	$(PGBIN)psql --list
config:
	vim $(PGDIR)postgresql.conf
pgpass:
$(HOMEDIR)/.pgpass:
	#echo 'hostname:port:mypgdatabase:mypguser:mypgpassword' >> $@
	#echo '/var/run/postgresql:5432:marcin:marcin:mypgpassword' >> $@
	echo '$(PGPIDDIR):5432:marcin:marcin:mypgpassword' >> $@
	chmod 600 $@


rsyslog:
	#sudo apt install rsyslog-pgsql
	-$(PGBIN)/psql --username=marcin -c 'DROP DATABASE "Syslog";'
	-$(PGBIN)/psql --username=marcin -c 'DROP USER rsyslog;'

	-createuser -d -S rsyslog
	-createdb --encoding=SQL_ASCII --template=template0 --host=$(PGPIDDIR) -O rsyslog -e Syslog

	-$(PGBIN)psql --username=marcin -c 'GRANT ALL ON DATABASE "Syslog" TO rsyslog ;'
	-$(PGBIN)psql --username=marcin -c 'ALTER DATABASE "Syslog" OWNER TO rsyslog ;'

