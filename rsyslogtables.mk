MAKEFLAGS 	+= --warn-undefined-variables
MAKEFLAGS 	+= --no-builtin-variables
MAKEFLAGS 	+= --no-builtin-rules

USERNAME := rsyslog
DBNAME   := Syslog

SHELL := psql
.SHELLFLAGS := --no-psqlrc --expanded --no-readline --host=/run/postgresql --username=rsyslog Syslog -c
.DEFAULT_GOAL := rsyslogtables

all:
	show client_encoding;
	show work_mem;
	show maintenance_work_mem;
conninfo:
	\conninfo

.ONESHELL:
rsyslogtables:
	CREATE TABLE SystemEvents (
	ID serial not null primary key,
	CustomerID bigint,
	ReceivedAt timestamp without time zone NULL,
	DeviceReportedTime timestamp without time zone NULL,
	Facility smallint NULL,
	Priority smallint NULL,
	FromHost varchar(60) NULL,
	Message text,
	NTSeverity int NULL,
	Importance int NULL,
	EventSource varchar(60),
	EventUser varchar(60) NULL,
	EventCategory int NULL,
	EventID int NULL,
	EventBinaryData text NULL,
	MaxAvailable int NULL,
	CurrUsage int NULL,
	MinUsage int NULL,
	MaxUsage int NULL,
	InfoUnitID int NULL ,
	SysLogTag varchar(60),
	EventLogType varchar(60),
	GenericFileName VarChar(60),
	SystemID int NULL
	);
	CREATE TABLE SystemEventsProperties (
	ID serial not null primary key,
	SystemEventID int NULL ,
	ParamName varchar(255) NULL ,
	ParamValue text NULL
	);
